package de.gymdorfen.theplague.gui;

import java.util.ArrayList;

import de.gymdorfen.theplague.Main;

public abstract class AbstractGUI {
	protected ArrayList<GUIElement> elements = new ArrayList<>();
	
	public Main render(Main wrapper) {
		for (GUIElement i : elements) {
			wrapper = i.render(wrapper);
		}
		return wrapper;
	}
	public void add(GUIElement element) {
		elements.add(element);
	}
}
