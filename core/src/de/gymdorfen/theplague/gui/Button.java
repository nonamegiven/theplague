package de.gymdorfen.theplague.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.gymdorfen.theplague.Main;

public class Button implements GUIElement {
	private int x;
	private int y;
	private int sizeX;
	private int sizeY;
	private String text;
	private final Runnable onClick;

	public Button(int x, int y, int sizeX, int sizeY, String text, Runnable onClick) {
		this.x = x;
		this.y = y;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.text = text;
		this.onClick = onClick;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSizeX() {
		return sizeX;
	}

	public void setSizeX(int sizeX) {
		this.sizeX = sizeX;
	}

	public int getSizeY() {
		return sizeY;
	}

	public void setSizeY(int sizeY) {
		this.sizeY = sizeY;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void onClick() {
		this.onClick.run();
	}

	@Override
	public void tick() {
		int mouseX = Gdx.input.getX(0);
		int mouseY = Gdx.input.getY(0);
		if (mouseX > this.x && mouseX < this.x + (this.x + this.sizeX) && mouseY > (this.y)
				&& mouseY < ((this.y + this.sizeY)) && Gdx.input.isButtonJustPressed(Buttons.LEFT)) {
			this.onClick();
		}
	}

	@Override
	public Main render(Main wrapper) {
		// window-mapped coordinates needed for y transform
		int yScreen = y * (Gdx.graphics.getHeight() / 480);
//		wrapper.batch.draw(new TextureRegion(wrapper.manager.get("button.png"), 1, 1), (float) (x),
//				(float) (Gdx.graphics.getHeight() - (yScreen + sizeYScreen)), (float) sizeX, (float) sizeYScreen);
//		wrapper.font.draw(wrapper.batch, text, x,
//				(Gdx.graphics.getHeight() - (yScreen + (sizeYScreen / 2 - wrapper.font.getCapHeight() / 2))));
		wrapper.batch.draw(new TextureRegion(wrapper.manager.get("button.png"), 1, 1), (float) (x),
				(float) (y), (float) sizeX, (float) sizeY);
		return wrapper;
	}

}
