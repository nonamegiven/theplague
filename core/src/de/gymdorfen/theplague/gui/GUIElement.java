package de.gymdorfen.theplague.gui;

import de.gymdorfen.theplague.Main;

public interface GUIElement {
	/**
	 * Takes over the game's shared resources to render a screen. Position. Size, etc. should be stored by the implementing class.
	 * 
	 * @param wrapper The game's resource wrapper.
	 * @return The modified resource wrapper.
	 * @author stahlhutf
	 */
	public Main render(Main wrapper);
	
	public default void tick() {
		return;
	}
}
