package de.gymdorfen.theplague.gui;

public class ExampleGUI extends AbstractGUI {
	public ExampleGUI() {
		super();
		this.add(new Button(0, 0, 64, 64, "Click Me!", () -> {
			System.out.println("Clicked!");
		}));
	}
	public void tick() {
		this.elements.forEach((element) -> {
			element.tick();
		});
	}
}
