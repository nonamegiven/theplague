package de.gymdorfen.theplague.map;

/**
 * Contains all tile types used in the game.
 * @author stahlhutf
 */
public enum Tiles {
	NONE(0),
	FLOORBOARDS(1),
	CONCRETE(2),
	WINDOW_TOP_LEFT(3),
	WINDOW_TOP_RIGHT(4),
	WINDOW_BOTTOM_LEFT(5),
	WINDOW_BOTTOM_RIGHT(6),
	CRATE(7),
	CARDBOARD_BOX(8);
	
	private final int id;

	private Tiles(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	public static Tiles lookupById(int id) {
		for (Tiles i : Tiles.values()) {
			if (i.getId() == id) {
				return i;
			}
		}
		return NONE;
	}
}
