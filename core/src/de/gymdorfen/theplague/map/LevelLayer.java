package de.gymdorfen.theplague.map;

/**
 * @author stahlhutf
 */
public class LevelLayer {
	private Tiles[][] map;
	private float scale;
	
	public LevelLayer(float scale, Tiles[][] map) {
		this.scale = scale;
		this.map = map;
	}

	public Tiles getTileAtPos(int x, int y) {
		return map[x][y];
	}
	public void setTileAtPos(int x, int y, Tiles tile) {
		this.map[x][y] = tile;
	}
	public Tiles[][] getTiles() {
		return map;
	}
	public float getScale() {
		return scale;
	}
	public void setScale(float scale) {
		this.scale = scale;
	}
}
