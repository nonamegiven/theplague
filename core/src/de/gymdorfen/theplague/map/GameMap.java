package de.gymdorfen.theplague.map;

import java.util.ArrayList;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 *Static class containing the game map information. It contains all {@link LevelLayer}s and {@link SpawnArea}s used in the game.
 *
 * @author stahlhutf
 */
public class GameMap {
	private static ArrayList<LevelLayer> layers = new ArrayList<>();
	private static int[] spawnPos = { 0, 0 };
	private static ArrayList<SpawnArea> spawnAreas = new ArrayList<>();

	public static int[] getSpawnPos() {
		return spawnPos;
	}
	/**
	 * Sets the spawn position of the player.
	 * 
	 * @param x
	 * @param y
	 * 
	 * @author stahlhutf
	 */
	public static void setSpawnPos(int x, int y) {
		try {
			spawnPos[0] = x;
			spawnPos[1] = y;
		} catch (ArrayIndexOutOfBoundsException e) {
			spawnPos = new int[] { x, y };
		}
	}
	/**
	 * Adds the specified {@link LevelLayer} to the layer stack at the specified index. If specified, the layer won't override other layers at the index.
	 * 
	 * @param index
	 * @param layer
	 * @param override
	 * 
	 * @author stahlhutf
	 */
	public static void setLayer(int index, LevelLayer layer, boolean override) {
		try {
			if (layers.get(index) != null && !override) {
				return;
			} else {
				layers.set(index, layer);
			}
		} catch (IndexOutOfBoundsException e) {
			layers.add(layer);
		}
	}
	
	/**
	 * Returns the layer stack as an array.
	 * 
	 * @return
	 */
	public static LevelLayer[] getLayers() {
		return layers.toArray(new LevelLayer[] {});
	}
	/**
	 * Adds a {@link SpawnArea} to the map. The area's spawners should be configured before passing it to this method.
	 * 
	 * @param area
	 * @author stahlhutf
	 */
	public static void addSpawnArea(SpawnArea area) {
		spawnAreas.add(area);
	}
	public static void removeSpawnArea(int index) {
		spawnAreas.remove(index);
	}

	/**
	 * Renders the map using the passed {@link SpriteBatch}. This method should only
	 * be called on the render thread. It is assumed that rendering has already
	 * started.
	 * 
	 * @author stahlhutf
	 * @param The {@link SpriteBatch} to use for rendering.
	 * @param The {@link AssetManager} containing the textures for the tiles.
	 */
	public static void render(AssetManager manager, SpriteBatch batch) {
		for (LevelLayer i : layers) {
			for (int j = 0; j < i.getTiles().length; j++) {
				for (int k = 0; k < i.getTiles()[j].length; k++) {
					switch (i.getTiles()[j][k]) {
					case NONE:
						break;
					case CONCRETE:
						// TODO: render actual textures once we have them
						Texture tex = manager.get("concrete wall.png", Texture.class);
						batch.draw(tex, j * i.getScale(), k * i.getScale(), tex.getWidth() * i.getScale(),
								tex.getHeight() * i.getScale());
						break;
					default:
						break;
					}
				}
			}
		}
	}
	/**
	 * Ticks all registered spawn areas using the specified {@linkplain Engine}.
	 * @param engine
	 * @author stahlhutf
	 */
	public static void tick(Engine engine) {
		spawnAreas.forEach((area) -> {
			area.tick(engine);
		});
	}
}
