package de.gymdorfen.theplague.map;

import java.util.ArrayList;

/**
 * Contains an unbaked tile map. To use this with {@link LevelLayer}s, call {@link UnbakedLevel#bake()}.
 * 
 * @author stahlhutf
 */
public class UnbakedLevel {
	private ArrayList<ArrayList<Integer>> tiles = new ArrayList<>();
	
	public UnbakedLevel(int rows) {
		for (int i = 0; i < rows ; i++) {
			tiles.add(new ArrayList<>());
		}
	}
	public UnbakedLevel() {
		this(1);
	}
	public void addTile(int xPos, int yPos, Tiles tile) {
		this.tiles.get(yPos).add(xPos, tile.getId());
	}
	public Tiles[][] bake() {
		Tiles[][] baked = new Tiles[tiles.get(0).size()][tiles.size()];
		for (int y = 0 ; y < tiles.size() ; y++) {
			for (int x = 0 ; x < tiles.get(y).size() ; x++) {
				baked[x][y] = Tiles.lookupById(tiles.get(y).get(x));
			}
		}
		return baked;
	}
}
