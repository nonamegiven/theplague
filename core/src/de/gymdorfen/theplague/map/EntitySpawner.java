package de.gymdorfen.theplague.map;

import com.badlogic.ashley.core.Entity;

/**
 * Specifies the arguments and return types for valid entity spawners used in {@linkplain SpawnArea}s.
 * 
 * @author stahlhutf
 */
@FunctionalInterface
public interface EntitySpawner {
	public Entity spawnEntity(float posX, float posY);
}
