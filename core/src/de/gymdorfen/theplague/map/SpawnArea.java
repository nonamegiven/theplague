package de.gymdorfen.theplague.map;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;

/**
 * Specifies an area at a given position of a given size in which enery update,
 * there is a chance for an entity to spawn. Position, size and spawn chance are
 * passed in the constructors. Each tick, there is a chance for a random entity spawner to be executed.
 * 
 * @author stahlhutf
 */
public class SpawnArea {
	private int[] pos;
	private int[] size;
	private float spawnChance;
	private ArrayList<EntitySpawner> spawners = new ArrayList<>();

	private Random random;

	public SpawnArea(int posX, int posY, int sizeX, int sizeY, float spawnChance) {
		this.pos = new int[] { posX, posY };
		this.size = new int[] { sizeX, sizeY };
		this.spawnChance = spawnChance;
		this.random = new Random();
	}

	public SpawnArea addSpawner(EntitySpawner spawner) {
		spawners.add(spawner);
		return this;
	}

	public Entity[] tick(Engine engine) {
		ArrayList<Entity> entities = new ArrayList<>();
		if (this.random.nextFloat(0, 1) <= this.spawnChance) {
			Entity entity = this.spawners.get(random.nextInt(this.spawners.size())).spawnEntity(
					this.random.nextFloat(this.size[0]) + (float) this.pos[0],
					this.random.nextFloat(this.size[1]) + (float) this.pos[1]);

			engine.addEntity(entity);
			entities.add(entity);
		}
		return entities.toArray(new Entity[] {});
	}

}
