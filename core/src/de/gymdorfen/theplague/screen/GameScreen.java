package de.gymdorfen.theplague.screen;

import java.util.ArrayList;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ScreenUtils;

import de.gymdorfen.theplague.Main;
import de.gymdorfen.theplague.config.PlayerConfig;
import de.gymdorfen.theplague.entity.component.EnemyComponent;
import de.gymdorfen.theplague.entity.component.EntityComponent;
import de.gymdorfen.theplague.entity.component.PositionComponent;
import de.gymdorfen.theplague.entity.system.PlayerInput;
import de.gymdorfen.theplague.gui.ExampleGUI;
import de.gymdorfen.theplague.map.GameMap;
import de.gymdorfen.theplague.map.LevelLayer;
import de.gymdorfen.theplague.map.Tiles;
import de.gymdorfen.theplague.map.UnbakedLevel;
import de.gymdorfen.theplague.util.Direction;
import de.gymdorfen.theplague.util.ZombieFactory;

/**
 * The game's main class.
 * 
 * @author stahlhutf
 */
public class GameScreen implements Screen {
	Main wrapper;

	static final int VWIDTH = 640;
	static final int VHEIGHT = 480;

	private ArrayList<Entity> entities;
	public static Entity player;

	final Family zombies = Family.all(EnemyComponent.class, PositionComponent.class).get();

	// TODO: add actual GUIs
	private ExampleGUI exampleGui = new ExampleGUI();

	Engine engine;

	public GameScreen(final Main wrapper) {
		this.wrapper = wrapper;
		this.engine = new Engine();
		this.entities = new ArrayList<>();

		wrapper.manager.load("concrete wall.png", Texture.class);
		wrapper.manager.load("button.png", Texture.class);
		wrapper.manager.load("monster normal.png", Texture.class);
		wrapper.manager.load("monster normal inverted.png", Texture.class);
		wrapper.manager.load("player.png", Texture.class);
		wrapper.manager.load("player inverted.png", Texture.class);

		wrapper.manager.finishLoading();

		// make the game map
		UnbakedLevel main = new UnbakedLevel();
		main.addTile(0, 0, Tiles.FLOORBOARDS);
		GameMap.setLayer(0, new LevelLayer(4, main.bake()), false);
		GameMap.setSpawnPos(0, 0);

		// TODO add spawn areas
//		GameMap.addSpawnArea(new SpawnArea(10, 10, 10, 10, 0.1f)
//				.addSpawner((x,y) -> {
//					return null;
//				}));

		// create the player
		player = new Entity()
				.add(new PositionComponent((float) GameMap.getSpawnPos()[0], (float) GameMap.getSpawnPos()[1]))
				.add(new EntityComponent(PlayerConfig.HEALTH, 1, Direction.RIGHT));
		engine.addEntity(player);
		this.entities.add(ZombieFactory.createZombie(10, 1));
		engine.addEntity(this.entities.get(0));
		
		engine.addSystem(new PlayerInput());
	}

	@Override
	public void show() {
	}

	@Override
	public void render(float delta) {
		GameMap.tick(engine);
		engine.update(delta);
		ScreenUtils.clear(0, 0, 0, 1);
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		ComponentMapper<PositionComponent> posMapper = ComponentMapper.getFor(PositionComponent.class);
		ComponentMapper<EntityComponent> entityMapper = ComponentMapper.getFor(EntityComponent.class);
		exampleGui.tick();
		wrapper.batch.begin();
		GameMap.render(wrapper.manager, wrapper.batch);
		if (entityMapper.get(player).facing == Direction.LEFT || entityMapper.get(player).facing == Direction.DOWN) {
			wrapper.batch.draw(new TextureRegion(wrapper.manager.get("player inverted.png", Texture.class)),
					posMapper.get(player).posX, posMapper.get(player).posY, 32, 64);
		} else if (entityMapper.get(player).facing == Direction.RIGHT || entityMapper.get(player).facing == Direction.UP) {
			wrapper.batch.draw(new TextureRegion(wrapper.manager.get("player.png", Texture.class)),
					posMapper.get(player).posX, posMapper.get(player).posY, 32, 64);
		}
		this.engine.getEntitiesFor(zombies).forEach((entity) -> {
			if (entityMapper.get(entity).facing == Direction.LEFT || entityMapper.get(entity).facing == Direction.DOWN) {
				wrapper.batch.draw(new TextureRegion(wrapper.manager.get("monster normal inverted.png", Texture.class)),
						posMapper.get(entity).posX, posMapper.get(entity).posY, 32, 64);
			} else if (entityMapper.get(entity).facing == Direction.RIGHT || entityMapper.get(entity).facing == Direction.UP) {
				wrapper.batch.draw(new TextureRegion(wrapper.manager.get("monster normal.png", Texture.class)),
						posMapper.get(entity).posX, posMapper.get(entity).posY, 32, 64);
			}
		});
//		wrapper = exampleGui.render(wrapper);
		wrapper.batch.end();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
	}
}
