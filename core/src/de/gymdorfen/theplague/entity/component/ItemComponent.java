package de.gymdorfen.theplague.entity.component;

import com.badlogic.ashley.core.Component;

public class ItemComponent implements Component {

	    private String id;
	    private String name;
	    private String description;
	    private int value;

	    public ItemComponent(String id, String name, String description, int value) {
	        this.id = id;
	        this.name = name;
	        this.description = description;
	        this.value = value;
	    }
	    public String getId() {
	        return id;
	    }

	    public void setId(String id) {
	        this.id = id;
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public int getValue() {
			return value;
		}
		public void setValue(int value) {
			this.value = value;
		}
	}


