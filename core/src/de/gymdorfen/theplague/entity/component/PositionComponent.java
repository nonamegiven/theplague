package de.gymdorfen.theplague.entity.component;

import com.badlogic.ashley.core.Component;

/**
 * @author stahlhutf
 */
public class PositionComponent implements Component {
	public float posX = 0.0f;
	public float posY = 0.0f;
	
	public PositionComponent(float x, float y) {
		this.posX = x;
		this.posY = y;
	}
}
