package de.gymdorfen.theplague.entity.component;

import com.badlogic.ashley.core.Component;

public class EnemyComponent implements Component {
	public float health = 100;

	public float attackPower = 10;

	public float speed = 2;

	public String enemyType;

	public String toString() {
		return "EnemyComponent{" + "health=" + health + ", attackPower=" + attackPower + ", speed=" + speed
				+ ", enemyType=" + enemyType + '}';
	}
}
