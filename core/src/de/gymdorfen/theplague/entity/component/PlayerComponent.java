package de.gymdorfen.theplague.entity.component;

import com.badlogic.ashley.core.Component;

public class PlayerComponent implements Component {
	private String id;
	private String name;

	public PlayerComponent(String id, String name) {
		this.id = id;
		this.name = name;
	}

	 public String getId() {
	        return id;
	    }

	    public void setId(String id) {
	        this.id = id;
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }
}
