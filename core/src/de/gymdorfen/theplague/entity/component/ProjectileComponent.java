package de.gymdorfen.theplague.entity.component;

import com.badlogic.ashley.core.Component;

public class ProjectileComponent implements Component {
    public float posX;
    public float posY;
    public int damage;
    public float speed;
    
    public ProjectileComponent(float x, float y, int damage, float speed) {
        this.posX = x;
        this.posY = y;
        this.damage = damage;
        this.speed = speed;
    }
}
