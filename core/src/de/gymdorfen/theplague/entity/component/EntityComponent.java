package de.gymdorfen.theplague.entity.component;

import com.badlogic.ashley.core.Component;

import de.gymdorfen.theplague.util.Direction;


/**
 * @author stahlhutf
 */
public class EntityComponent implements Component {
	public int maxHealth;
	public int health;
	public Direction facing;
	
	public EntityComponent(int maxHealth, float healthPercent, Direction facing) {
		this.maxHealth = maxHealth;
		this.health = (int) Math.ceil(maxHealth * healthPercent);
		this.facing = facing;
	}
}
