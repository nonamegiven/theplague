package de.gymdorfen.theplague.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;

import de.gymdorfen.theplague.entity.component.EnemyComponent;
import de.gymdorfen.theplague.entity.component.EntityComponent;
import de.gymdorfen.theplague.entity.component.PlayerComponent;

public class Meleeatack extends EntitySystem {
    private ComponentMapper<PlayerComponent> playerMapper = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<EnemyComponent> enemyMapper = ComponentMapper.getFor(EnemyComponent.class);
    private ComponentMapper<EntityComponent> entityMapper = ComponentMapper.getFor(EntityComponent.class);

    public Meleeatack() {
        super();
    }

    @Override
    public void update(float deltaTime) {
    
        Family playerEnemyFamily = Family.all(PlayerComponent.class, EnemyComponent.class).get();
        for (Entity entity : getEngine().getEntitiesFor(playerEnemyFamily)) {
            PlayerComponent player = playerMapper.get(entity);
            EnemyComponent enemy = enemyMapper.get(entity);

            enemy.health -= 5;

       
            if (enemy.health <= 0) {
           
                getEngine().removeEntity(entity);
            }
        }
        Family enemyPlayerFamily = Family.all(EnemyComponent.class, PlayerComponent.class).get();
        for (Entity entity : getEngine().getEntitiesFor(enemyPlayerFamily)) {
            EnemyComponent enemy = enemyMapper.get(entity);
            PlayerComponent player = playerMapper.get(entity);
            EntityComponent playerEntity = entityMapper.get(entity);

    
            playerEntity.health -= enemy.attackPower;

            if (playerEntity.health <= 0) {
    
                System.out.println("Game Over! Player defeated by enemy.");
            }
        }
    }

    @Override
    public boolean checkProcessing() {
        return true;
    }

    @Override
    public void setProcessing(boolean processing) {
      
    }
}