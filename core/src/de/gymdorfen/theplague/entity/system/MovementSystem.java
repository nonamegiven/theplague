package de.gymdorfen.theplague.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;

import de.gymdorfen.theplague.entity.component.PositionComponent;

public class MovementSystem extends EntitySystem {
    private ImmutableArray<Entity> entities;

    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);

    private float speedX = 0;
    private float speedY = 0;
    private static final float SPEED = 100.0f;

    public MovementSystem() {}

    @Override
    public void addedToEngine(Engine engine) {
        entities = engine.getEntitiesFor(Family.all(PositionComponent.class).get());
    }

    @Override
    public void update(float deltaTime) {
        for (int i = 0; i < entities.size(); ++i) {
            Entity entity = entities.get(i);
            PositionComponent position = pm.get(entity);
            
            position.posX += speedX * deltaTime;
            position.posY += speedY * deltaTime;
        }
    }

    public void handleInput(boolean up, boolean down, boolean left, boolean right) {
        speedX = 0;
        speedY = 0;
        if (up) speedY = SPEED;
        if (down) speedY = -SPEED;
        if (left) speedX = -SPEED;
        if (right) speedX = SPEED;
    }
}