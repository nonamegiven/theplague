package de.gymdorfen.theplague.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

import de.gymdorfen.theplague.config.PlayerConfig;
import de.gymdorfen.theplague.entity.component.EntityComponent;
import de.gymdorfen.theplague.entity.component.PositionComponent;
import de.gymdorfen.theplague.screen.GameScreen;
import de.gymdorfen.theplague.util.Direction;

public class PlayerInput extends EntitySystem {
	public ComponentMapper<PositionComponent> posMapper = ComponentMapper.getFor(PositionComponent.class);
	public ComponentMapper<EntityComponent> entityMapper = ComponentMapper.getFor(EntityComponent.class);

	@Override
	public void update(float deltaTime) {
		super.update(deltaTime);
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			entityMapper.get(GameScreen.player).facing = Direction.LEFT;
			posMapper.get(GameScreen.player).posX -= PlayerConfig.SPEED * deltaTime;
		} else if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			entityMapper.get(GameScreen.player).facing = Direction.RIGHT;
			posMapper.get(GameScreen.player).posX += PlayerConfig.SPEED * deltaTime;
		} else if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			entityMapper.get(GameScreen.player).facing = Direction.DOWN;
			posMapper.get(GameScreen.player).posY -= PlayerConfig.SPEED * deltaTime;
		} else if (Gdx.input.isKeyPressed(Keys.UP)) {
			entityMapper.get(GameScreen.player).facing = Direction.UP;
			posMapper.get(GameScreen.player).posY += PlayerConfig.SPEED * deltaTime;
		}
	}
}