package de.gymdorfen.theplague.util;

/**
 * The four directions an entity can be facing.
 * @author stahlhutf
 */
public enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT
}
