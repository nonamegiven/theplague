package de.gymdorfen.theplague.util;

import com.badlogic.ashley.core.Entity;

import de.gymdorfen.theplague.entity.component.EnemyComponent;
import de.gymdorfen.theplague.entity.component.EntityComponent;
import de.gymdorfen.theplague.entity.component.PositionComponent;

public class ZombieFactory {
	public static Entity createZombie(float health, float speed) {
		Entity zombie = new Entity();
		zombie.add(new PositionComponent(0, 0)).add(new EntityComponent(10, 1f, Direction.LEFT))
				.add(new EnemyComponent());
		return zombie;
	}
}